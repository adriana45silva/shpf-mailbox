# Improvements

- Add Storybook for components
- Add PropTypes checking for testing
- Add linters for CSS/JS (Prettier/ESLint with TS configurations)
- Add testing for wrapped components
- Add Precommit with linters
- Add CSS minification
- Add HTML minification
- Add more postcss plugins/utils
- Add tests with Jest snapshots
- UI enhancements
- CSS style separation by components (sidebar/email renderer) or scoped styling