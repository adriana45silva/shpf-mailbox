import React from 'react'
import {App} from './App'
import {shallow} from 'enzyme'
import {RootStore} from 'typescript/stores/index'

describe('<App/>', () => {

  const rootStore = new RootStore()


  let wrapper = shallow(
    <App rootStore={rootStore}/>
  )

  it('should be defined', () => {
    expect(App).toBeDefined();
  })

  it('should render main section', () => {
    expect(wrapper.find('.mb__main').exists()).toBe(true)
  })
  
  
})