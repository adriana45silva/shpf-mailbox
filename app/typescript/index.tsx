import React from 'react'
import {render} from 'react-dom'
import {AppContainer} from 'react-hot-loader'
import {App} from 'typescript/App'
import {Provider} from 'mobx-react'
import {RootStore} from 'typescript/stores/index'
import 'stylesheets/index'


const root = document.getElementById('app')
const store = new RootStore()

// Render the component initially
render(
  <AppContainer>
    <Provider {...store}>
      <App rootStore={store}/>
    </Provider>
  </AppContainer>,
  root
)

// Render the component in case of a hot replacement
// This is removed automatically in production
if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default

    render(
      <AppContainer>
        <Provider {...store}>
          <NextApp rootStore={store}/>
        </Provider>
      </AppContainer>,
      root
    )
  })
}
