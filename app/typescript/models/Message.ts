export interface IMessage {
  messages: Array<Messages>
}

export type Messages = {
  uid: string
  sender: string
  subject: string
  message: string
  time_sent: number
  read?: boolean
}