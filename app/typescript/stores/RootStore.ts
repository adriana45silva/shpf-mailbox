import {MessageStore} from 'typescript/stores/index'
import bowser from 'bowser'

export default class RootStore {
  messages = new MessageStore()
  bowser = bowser ? bowser : {}
}
