import {action, observable, set, remove} from 'mobx'
import { IMessage, Messages } from 'typescript/models/Message';
import messagesJson from 'json/messages_sample.json'
import {IObservableArray} from 'mobx'

export default class MessageStore {
  constructor(){}

  @observable allMessages: IMessage = messagesJson
  @observable currentMessage: Messages
  @observable isMessageOpened: boolean
  @observable openedMessages: Array<any> = []
  @observable remainingUnreadMessages: Array<Messages>

  @action findMsgIndex(message: Messages): number {
    return this.allMessages.messages.findIndex((id: any) => id.uid === message.uid)
  }

  @action openMessage (message: Messages){
    if (message) {
      this.openedMessages.push(message)
      this.currentMessage = message
      let messageIndex = this.findMsgIndex(message)
      
      set(this.allMessages.messages[messageIndex], 'read', true)
      this.remainingMessagesCount()
    }
  }

  @action remainingMessagesCount () {
    const {messages} = this.allMessages
    this.remainingUnreadMessages = messages.filter((msg: any) => !msg.read)
  }

  @action removeMessages (message: Messages) {
    this.currentMessage = null
    if (this.allMessages.messages.length){
      let messages = this.allMessages.messages as IObservableArray
      let nextMsg = this.allMessages.messages[this.findMsgIndex(message) + 1]
      this.openMessage(nextMsg)
      this.remainingMessagesCount()
  
      if (this.findMsgIndex(message) === -1){
        this.currentMessage = null
      }
  
      return messages.remove(message)
    }
  }
}