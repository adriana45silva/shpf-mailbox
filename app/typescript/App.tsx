import React, { SFC} from 'react'
import {observer} from 'mobx-react'
import {Sidebar, EmailRenderer} from 'typescript/components/index'
import {RootStore} from 'typescript/stores/index'

interface AppProps {
  rootStore?: RootStore
}

export const App: SFC<AppProps> = observer(({rootStore, ...props}) => {

  return (
    <section className="mb__main">
      <div className="mb__wrapper">
        <Sidebar emailStore={rootStore.messages}/>
        <EmailRenderer message={rootStore.messages}/>
      </div>
    </section>    
  )
})