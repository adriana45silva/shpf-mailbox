import React from 'react'
import {EmailRenderer} from './EmailRenderer'
import {shallow, mount} from 'enzyme'

describe('<EmailRenderer/>', () => {

  let message: any = {
    currentMessage: null,
    removeMessages: () => false,
    allMessages: {
      messages: [
        {
          "uid": "21",
          "sender": "Ernest Hemingway",
          "subject": "animals",
          "message": "This is a tale about nihilism.", 
          "time_sent": 1459239867
        }   
      ]
    }    
  }

  let wrapper = shallow(
    <EmailRenderer message={message}/>
  )

  it('should be defined', () => {
    expect(EmailRenderer).toBeDefined();
  })
  
  it('should render select message component if current message is null', () => {
    expect(wrapper.find('.mb__email-proto').exists()).toBe(true)
  }) 
  
  it('should render received messages', () => {
    message.currentMessage = {
      "uid": "21",
      "sender": "Ernest Hemingway",
      "subject": "animals",
      "message": "This is a tale about nihilism.", 
      "time_sent": 1459239867      
    }
    const wrapper = shallow(
      <EmailRenderer message={message}/>
    )      
    expect(wrapper.find('.mb__email-msg').exists()).toBe(true)
  })
  
  it('should delete message', () => {
    message.currentMessage = {
      "uid": "21",
      "sender": "Ernest Hemingway",
      "subject": "animals",
      "message": "This is a tale about nihilism.", 
      "time_sent": 1459239867      
    }

    const wrapper = mount(
      <EmailRenderer message={message}/>
    )

    expect(wrapper.find('.fa-trash-alt').exists()).toBe(true)
    wrapper.find('.fa-trash-alt').simulate('click')
    wrapper.setProps({emailStore: {
      ...message,
      allMessages: {messages: []},
      remainingUnreadMessages: [],
      currentMessage: null
    }})    
    expect(wrapper.props().emailStore.allMessages.messages.length).toBeLessThanOrEqual(0)
    expect(wrapper.props().emailStore.remainingUnreadMessages.length).toBeLessThanOrEqual(0)
    expect(wrapper.props().emailStore.currentMessage).toBeFalsy()
  })  
})