import React, {SFC} from 'react'
import {observer} from 'mobx-react'
import {EpochToDate} from 'typescript/utils/EpochToDate'
import { Messages } from 'typescript/models/Message'

interface EmailRendererProps {
  message?: Messages
}

export const EmailRenderer: SFC<EmailRendererProps> = observer(({message}) => {
  if (message) {
    const {currentMessage} = message

    const noEmailSelected = () => {
      return (
        <div className="mb__email-proto">
          <h2>Welcome to Mailbox Prototype!</h2>
          <p>Select a message on the sidebar to visualize it</p>
          <p>Made by <a href="https://github.com/adriana45silva" target="_blank">@adriana45silva</a> 👩‍💻</p>
        </div>
      )
    } 
  
    return (
      <section className="mb__email">
        { currentMessage ? 
          (
            <div>
              <header>
                <ul>
                  <li>
                    <h3 className="text-capitalize">{currentMessage.subject}</h3>                  
                    <span>
                      <i className="far fa-trash-alt" onClick={() => message.removeMessages(currentMessage)}></i>
                    </span>                
                  </li>
                  <li>
                    <span className="text-capitalize">{currentMessage.sender}</span>
                  </li>
                  <li>
                    <span className="text-capitalize">{EpochToDate(currentMessage.time_sent, 'DD/MM/YYYY - HH:MM')}</span>                  
                  </li>
                </ul>                          
              </header>
              <div className="mb__email-msg">
                {currentMessage.message}
              </div>
            </div>
          ) :
          noEmailSelected()
        }
      </section>
    )      
  }

})