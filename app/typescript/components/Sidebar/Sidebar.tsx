import React, {Component} from 'react'
import {observer} from 'mobx-react'
import MessageStore from 'typescript/stores/MessageStore'
import {EpochToDate} from 'typescript/utils/EpochToDate'
import InlineSVG from 'svg-inline-react';
const logo = require('../../../assets/shopify.svg') as string;

interface SidebarProps {
  emailStore?: MessageStore
}

@observer
export class Sidebar extends Component<SidebarProps> {

  componentDidMount(){
    if (this.props.emailStore) {
      this.props.emailStore.remainingMessagesCount()
    }
  }

  renderMessages(){
    if (this.props.emailStore) {
      const {allMessages} = this.props.emailStore
      const {emailStore} = this.props
  
      return allMessages.messages.map((el, index) => (
        <li key={index} className="mb__sidebar-item">
          <div className={`${!el.read ? 'mb__sidebar-item--unread' : ''} mb__sidebar-item__title`} onClick={() => emailStore.openMessage(el)}>
            <span className="text-capitalize">{el.subject}</span>
            <span className="text-capitalize">{el.sender}</span>
            <span className="text-capitalize">{EpochToDate(el.time_sent)}</span>         
          </div>
          <div className="mb__sidebar-item__trash">
            <i className="far fa-trash-alt" onClick={() => emailStore.removeMessages(el)}></i>
          </div>
        </li>
      ))
    }
  }

  render() {
    if (this.props.emailStore) {
      const {remainingUnreadMessages} = this.props.emailStore 

      return (
        <aside className="mb__sidebar">
          <header>
            <InlineSVG src={logo}/>
          </header>
          <div className="mb__sidebar-title">
            <strong>Unread</strong>
            <span className="badge">{remainingUnreadMessages && remainingUnreadMessages.length}</span>
          </div>
          <nav>
            <ul>
              {this.renderMessages()}
            </ul>          
          </nav>
        </aside>
      )      
    }
  }
}