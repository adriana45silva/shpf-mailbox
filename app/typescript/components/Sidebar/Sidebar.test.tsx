import React from 'react'
import {Sidebar} from './Sidebar'
import {shallow, mount} from 'enzyme'
import getType from 'jest-get-type'
import {MessageStore} from 'typescript/stores/index'

describe('<Sidebar/>', () => {

  const messageStore = new MessageStore()

  let messages = {
    currentMessage: null,
    isMessageOpened: false,
    openedMessages: [],
    remainingUnreadMessages: [
      {
        "uid": "22",
        "sender": "Stephen King",
        "subject": "adoration",
        "message": "The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. ",
        "time_sent": 1459248747
      }      
    ],
    allMessages: {
      messages: [
        {
          "uid": "21",
          "sender": "Ernest Hemingway",
          "subject": "animals",
          "message": "This is a tale about nihilism.", 
          "time_sent": 1459239867
        }   
      ]
    },
    findMsgIndex: () => 0,
    openMessage: () => null,
    remainingMessagesCount: () => null,
    removeMessages: () => false
  }

  let mountedWrapper = mount(
      <Sidebar emailStore={messages}/>
  )

  it('should be defined', () => {
    expect(Sidebar).toBeDefined();
  })

  it('should do an initial render with props', () => {
    expect(mountedWrapper.find('.mb__sidebar').exists()).toBe(true)
    expect(mountedWrapper.props().emailStore).toBeDefined()
    expect(getType(mountedWrapper.props().emailStore.allMessages.messages)).toBe('array')
    expect(mountedWrapper.find('.mb__sidebar-item').exists()).toBe(true)
  })


  it('should change item class, increase opened messages and decrease unread messages', () => {
    expect(mountedWrapper.find('.mb__sidebar-item--unread').exists()).toBe(true)
    mountedWrapper.find('.mb__sidebar-item--unread').simulate('click')
    mountedWrapper.setProps({emailStore: {
      ...messages,
      openedMessages: [
        {
          "uid": "21",
          "sender": "Ernest Hemingway",
          "subject": "animals",
          "message": "This is a tale about nihilism.", 
          "time_sent": 1459239867
        },  
      ],
      allMessages: {messages: [
        {
          "uid": "21",
          "sender": "Ernest Hemingway",
          "subject": "animals",
          "message": "This is a tale about nihilism.", 
          "time_sent": 1459239867,
          read: true
        }        
      ]},
      remainingUnreadMessages: []
    }})
    expect(mountedWrapper.find('.mb__sidebar-item').hasClass('mb__sidebar-item--unread')).toBe(false)
    expect(mountedWrapper.props().emailStore.openedMessages.length).toBeGreaterThan(0)
    expect(mountedWrapper.props().emailStore.remainingUnreadMessages.length).toBeLessThanOrEqual(0)
  })

  it('should delete message', () => {
    expect(mountedWrapper.find('.mb__sidebar-item').exists()).toBe(true)
    mountedWrapper.find('.fa-trash-alt').simulate('click')
    mountedWrapper.setProps({emailStore: {
      ...messages,
      allMessages: {messages: []},
      remainingUnreadMessages: []
    }})    
    expect(mountedWrapper.props().emailStore.allMessages.messages.length).toBeLessThanOrEqual(0)
    expect(mountedWrapper.props().emailStore.remainingUnreadMessages.length).toBeLessThanOrEqual(0)
  })
 
})