import format from 'date-fns/format'

export function EpochToDate(epochTimestamp: number, formatString: string = 'dddd D MMM, HH:mm'): string {
  if (epochTimestamp){
    let toJsDate = new Date(epochTimestamp * 1000)
    return format(toJsDate, formatString)    
  }

  return
}