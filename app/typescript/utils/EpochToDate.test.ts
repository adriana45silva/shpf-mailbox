import {EpochToDate} from './EpochToDate'
import getType from 'jest-get-type'


describe('EpochToDate function', () => {
  let timestamp = 1459239867
  let format = 'DD/MM/YYYY'


  it('should return undefined if first param is null', () => {
    expect(EpochToDate(null)).toBeUndefined()
  })

  it('should return undefined if first param is undefined', () => {
    expect(EpochToDate(undefined)).toBeUndefined()
  })
  
  it('should return a string only with one param', () => {
    expect(getType(EpochToDate(timestamp, format))).toBe('string')
  })   

  it('should return a string with both params passed', () => {
    expect(getType(EpochToDate(timestamp, format))).toBe('string')
  })   
})