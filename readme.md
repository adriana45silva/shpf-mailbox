# Shopify Mailbox Component - by Adriana Silva  👩‍💻

## Install 📦

`npm install`

## Running locally ⚙️

`npm run dev`

## Running tests 💉

Tests automatically runs with local running or building, but if you want to run just the tests, just run:

`npm run test`

## Live demo 🚀

https://shpf-mailbox.netlify.com/


## Improvement and approach notes 📝

The [improvements note](https://bitbucket.org/adriana45silva/shpf-mailbox/src/master/improvements.md) and the [approach note](https://bitbucket.org/adriana45silva/shpf-mailbox/src/master/approach.md) requested are also on the root of this repository


