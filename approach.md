# Approach

First and foremost, I assumed that this assignment would be way longer in terms of things to do and in fact I am kind of concerned that I could be doing less than asked or there's somewhat a trick hidden that I bypassed. 

My line of thought is to list the MVP deliverable and from that trying to improve what I could do along the way. I've mounted the dev server/build structure myself as well as the CSS structure because sometimes using a boilerplate (like create-react-app) could be a lot like dealing with a Troy Horse, there's way too many configurations that you won't be using and you can't easily remove.

So with that in mind I made a simple component comunication, with all the actions/state modifications into one single component (the sidebar component), and have the others only to reflect the passed state (stateless components). It could be done without stores and with one and only "smart" component passing the state down, but I structured the things assuming this could scale.

Since I believe to proper test the providers/stores (wrapped) could be too time consuming I've only tackled the basic testing of the functionalities (opening, deleting, listing the messages) from the components. That's why sidebar doesn't have an `inject()` function/HOC, but could totally use one (and the implementation would be similar).