// ------------------------------------------------------------------ 
// Default Dependencies
// ------------------------------------------------------------------

const { resolve } = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// ------------------------------------------------------------------
// Environment Configs
// ------------------------------------------------------------------

const devEnv = process.env.NODE_ENV === 'development';
const prodEnv = process.env.NODE_ENV === 'production';
const hotLoader = 'react-hot-loader/patch';

// ------------------------------------------------------------------
// Configs
// ------------------------------------------------------------------


const HtmlConfigs = {
  title: 'Webpage title',
  template: resolve(__dirname, 'app/templates/index.html')
};


// ------------------------------------------------------------------

const plugins = () => {
  let arr = [];
  arr.push(new HtmlWebpackPlugin(HtmlConfigs));
  devEnv
    ? arr.push(...pluginsDev())
    : arr.push(...pluginsProd());
  return arr;
};

const pluginsDev = () => {
  return [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new webpack.NamedModulesPlugin()
  ];
};

const pluginsProd = () => {
  return [
    new UglifyJsPlugin({
      uglifyOptions: {
        comments: false
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new MiniCssExtractPlugin({filename:'styles_[hash].css'})
  ];
};

// ------------------------------------------------------------------

const entries = () => {
  let entries = [];

  if (devEnv) {
    entries.unshift(hotLoader);
  }

  entries.push(resolve(__dirname, 'app/typescript/index.tsx'));
  return entries;
};

// ------------------------------------------------------------------

const sassLoader = () => {
  return prodEnv
    ? sassLoaderProd()
    : sassLoaderDev();
};

const sassLoaderDev = () => {
  let sassLoaderDev = [
    'style-loader',
    {
      loader: 'css-loader?sourceMap'
    },
    {
      loader: 'sass-loader?sourceMap',
      options: {
        includePaths: [
          resolve(__dirname, 'source/stylesheets'),
          resolve(__dirname, 'node_modules')
        ]
      }
    },
    {
      loader: 'postcss-loader',
      options: {
        config: {
          path: resolve(__dirname, './postcss.config.js')
        },
        sourceMap: devEnv,
        plugins: [
          require('autoprefixer')()
        ]        
      }
    }
  ];

  return sassLoaderDev;
};

const sassLoaderProd = () => {
  let sassLoaderProd = [
    MiniCssExtractPlugin.loader,
    { loader: 'css-loader' },
    {
      loader: 'sass-loader',
      options: {
        includePaths: [
          resolve(__dirname, 'app/stylesheets'),
          resolve(__dirname, 'node_modules')
        ]
      }
    },
    { loader: 'postcss-loader' }
  ]

  return sassLoaderProd;
};

// ------------------------------------------------------------------

let config = {
  mode: devEnv ? 'development' : 'production',
  entry: entries(),
  output: {
    filename: 'bundle_[hash].js',
    path: resolve(__dirname, 'dist'),
    publicPath: '/'
  },
  context: resolve(__dirname, 'source'),
  devtool:
    prodEnv
      ? 'source-map'
      : 'cheap-module-source-map',
  stats: 'errors-only',
  devServer: {
    hot: true,
    host: '0.0.0.0',
    port: 3000,
    historyApiFallback: true,
    publicPath: '/',
    disableHostCheck: true,
    openPage: '',
    stats: 'errors-only'
  },
  plugins: plugins(),
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: ['react-hot-loader/webpack', 'ts-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        use: sassLoader()
      },
      {
        test: /\.(ttf|eot|otf|woff(2)?)(\w+)?$/,
        use: 'file-loader?name=[path][name].[ext]'
      },
      {
        test: /\.(svg)$/i,
        use: 'svg-inline-loader'
      }      
    ]
  },
  resolve: {
    extensions: [
      '.js',
      '.jsx',
      '.json',
      '.scss',
      '.css',
      '.svg',
      '.pdf',
      '.zip',
      '.ts',
      '.tsx'
    ],
    alias: {
      stylesheets: resolve(__dirname, 'app/stylesheets'),
      typescript: resolve(__dirname, 'app/typescript'),
      components: resolve(__dirname, 'app/typescript/components'),
      json: resolve(__dirname, 'app/json')
    }
  }
};

// ------------------------------------------------------------------

module.exports = config;